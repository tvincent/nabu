## About

Brief description of the merge request: what it brings, why it is necessary, which issues it will close.

## To do

  - [ ] Implement feature (class and docstring)
  - [ ] Unit tests
  - [ ] Integrate in pipeline
  - [ ] End-to-end reconstruction test


## Notes

Additional notes, ex. implementation details.
