# Change Log

## 2021.2.0

This version brings major changes in the API to make it more consistent. Most of the pre-2021.2.0 API should work, although possibly with a deprecation warning.

### Application

  - Half tomography does not require an even number of angles anymore
  - Added composite sinogram/radio Center of Rotation estimator
  - The new pipeline (handling full radios) has now a Cuda backend, except for distortion correction.
  - Fix `enable_halftomo = auto` not working when FOV == "full"
  - Renamed configuration key 'flatfield_enabled' to 'flatfield'. The following modes are available:
     - False: disable flatfield normalization
     - True: attempt at loading "reduced" flats/darks, compute if no file available
     - "force-compute": discard any existing file with pre-computed flats/darks, do the computations anyway
     - "force-load": Force to use existing flats/darks from a file, regardless of the input dataset
  - Unknown configuration keys are now ignored by default (instead of making nabu exit)

### Library

  - Create `nabu.pipeline` which will contain all pipeline implementations (full-field, XRD-CT, ...). Move modules (ex. from `nabu.resources`).
  - Rename pipelines with more consistent names: `Chunked` and `Grouped`.
  - `nabu.preproc`: `sinogram` and `rings` were moved to `nabu.reconstruction`
  - Add `nabu.preproc.phase_cuda.CudaCTFPhaseRetrieval`
  - Add `nabu.cuda.binning` which offers a generic Cuda padding utility


## 2021.1.1

### Application

   - Added `unsharp_method` to select unsharp mask method (gaussian or log)
   - `enable_halftomo` can now be set to `auto` to let nabu determine if half aqcuisition was used
   - Add `nabu-validator` command-line tool

### Library

   - `scipy` is now a mandatory dependency


## 2021.1.0

### Application

  - Add sinogram-based center of rotation estimation methods.
  - Add projections rotation and detector tilt estimation.
  - Add support from dumping processing steps into a file, and to resume a pipeline from a saved step.
  - Add sinogram-based rings artefacts removal method
  - Add  CTF single-distance phase retrieval
  - Add support for flats distortion estimation and correction
  - Add EDF output data format

### Command-line interface tools

  - Add `nabu-rotate` for performing a rotation of each projection in a dataset.
  - Add `nabu-double-flatfield` to compute the double flatfield image from projections.

## Library

  - `PaganinPhaseRetrieval` length parameters are now expressed in meters.
  - Add `nabu.estimation` module
  - The `preproc.FlatField` class now takes arrays as input instead of `DataUrl`. The class `FlatFieldDataUrls` can be used as a replacement.

## 2020.5.0

Major version (November 2020).

### Application

  - Add cuda backend for histogram, also for the `nabu-histogram` command.
  - Enable histogram computation for other output formats than HDF5 (ex. tiff)
  - The files "nabu_processes.h5" and "nabu.log" are now named as a function of the dataset prefix. This prevents conflicts when several datasets are reconstruction simultaneously from the same folder.
  - Add nabu-generate-info CLI tool, which generates a ".info" for pluggin ESRF legacy pipeline post-processing tools.
  - Add a utility to exclude projections from being processed: `exclude_projections` in `[dataset]`
  - Add new methods for finding the center of rotation
  - Mitigate issue of nabu hanging forever to allocate memory on big data chunks
  - Fix unsharp mask when used without phase retrieval
  - When using HDF5 datasets, a custom entry in the file can now be specified using the `hdf5_entry` key in the `[dataset]` section in nabu configuration.


### Library

  - Fix half-tomography sinograms when center of rotation is on the left
  - Add misc.histogram_cuda: Cuda backend for partial volume histograms
  - Add `preproc.alignment.CenterOfRotationAdaptiveSearch`, `preproc.alignment.CenterOfRotationSlidingWindow` and `preproc.alignment.CenterOfRotationGrowingWindow` for automatic CoR estimation.
  - Add support for `h5py >= 3.0`, although it requires `silx >= 0.14`


## 2020.4.2
This version brings new features and fixes.

### Application

  - Center of Rotation is not incorrectly shifted anymore when set on the left in "normal acquisition mode"

## 2020.4.1

This is a minor version bringing some fixes in the application.

### Application

  - In the HDF5-NX dataset file, the pixel size taken by nabu is now `magnified_pixel_size` instead of `pixel_size`. This will have notable consequences for phase retrieval (`delta_beta` will be much smaller to obtain the same effect).
  - The `nabu-config` command line does not need the `--bootstrap` flag anymore, as it is not the default behavior.
   - Configuration file: angles in `angles_files` have to be provided in degrees (as for `angles_offset`)
   - Fixed some comments in configuration file
   - A more helpful error will be returned if either flat/dark is missing from NX file.


## 2020.4.0

This is a version adds a number of features.

### Application

  - Automatic center of rotation estimation, working for both half-acquisition and regular scans
  - Command line tool for splitting a NXTomo file by "z" series
  - Volume histogram. Command line tool for merging histogram of multiple volumes.
  - Enable to perform flat-field normalization with darks/flats from another dataset
  - Sinogram normalization (baseline subtraction)
  - Nabu does not need `tomwer_processes.h5` to get the "final" darks/refs anymore.
  - Fix `fbp_filter_type = none`: now it will actually disable any filtering
  - Fix auto-CoR not working when flatfield is disabled

### Library

  - Add `misc.histogram` for computing partial histograms and merging them
  - Add `preproc.sinogram.SinoNormalization`
  - Fix double flat-field when `dff_sigma > 0`  which was giving nonsense results.
  - Half-tomography: add support for center of rotation on the left side

## 2020.3.0

This is a release for ESRF User Service Mode restart.

The main new feature is the full volume reconstruction. The reconstruction is done in multiple stages by dividing the volume in  sub-volumes, which are processed and stitched together in the end. 

### Application

  - Multi-stage volume reconstruction with possibly overlapping chunks (`LocalReconstruction` in `nabu.app`).  Chunk sizes are computed automatically as a function of available memory. Phase margin is computed automatically as a function of phase retrieval parameters. HDF5 files corresponding to each chunk are merged as a HDF5 master file.
  - Basic support for tiff output format (float32, single frames).
  - Basic support for jpeg2000 output format (uint16, single frames). For now only lossless compression is supported. 
  - Integrate "CCD correction" (thresholded median filter) in pipeline
  - Command Line Interface improvements: `nabu nabu.conf --slice middle`,  add options `--cpu_mem_fraction` and `--gpu_mem_fraction`.

### Library

   - Add alignment utilities: `nabu.preproc.alignment`. There are many use cases, please see the corresponding documentation pages for more information.
   - Flat-field: support more than two flats
   - Unify API of `CCDCorrection` and `CudaCCDCorrection`


## 2020.2.0

This is a "preview release", preparing an important release foreseen for September 2020.

### Library features

  - Vertical translations
  - Double flat-field (radios-based rings artefacts removal)
  - Support for half tomography
  - `preproc.alignment` module
  - Support for flat-field normalization with more than 2 flats

### Application
  - Take configuration file `start_z` and `end_z` into account for `nabu` CLI
  - Phase retrieval: `method = none` is now default (no phase retrieval by default)
  - Add `overwrite_results` 
  - Add support for `rotation_axis_position = auto` (except for half-tomo)

### Internal
  - Re-wrote internal processing pipeline with a much simpler design
  - NXresults: config is now a H5 dataset. "image stack" default interpretation.


## 2020.1.0: 2020/04/29

This is the first official release of Nabu. Please see the [features overview](www.silx.org/pub/nabu/doc/features.html)
