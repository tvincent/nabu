# Nabu command line interface

On top of the nabu library, applications can be built for specific purposes. Currently, `nabu.app`provides a reconstruction pipeline for full-field data.

## Generating a configuration file

Please see the [documentation on configuration file](nabu_config_file.md).

## Running the reconstruction pipeline

The user entry point for running the reconstruction pipeline is the command line `nabu`.

Running `nabu --help` gives an overview of the syntax. `nabu --version` returns the current installed version.

The command line `nabu` is always used with a configuration file. When you have created a configuration file `nabu.conf`, then running
```bash
nabu nabu.conf
```
will reconstruct all the volume on the local machine, except if `start_z` and/or `end_z` are modified in `nabu.conf`. In any case, you can overwrite these configuration file values. Fo example:

```bash
nabu nabu.conf --slice 500-599
```
will reconstruct 100 slices: 500, 501, 502, ..., 599 included.


```{warning}
Indexing is zero-based. This means that to reconstruct the first slice, you have to run nabu nabu.conf --slice 0 (or put start_z = 0 in the configuration file)
```

## Examples

### Reconstruct a single slice

Reconstruct the first slice:

```bash
nabu nabu.conf --slice 0
```
or equivalently:
```bash
nabu nabu.conf --slice first
```

Reconstruct the middle slice:
```bash
nabu nabu.conf --slice middle
```

Reconstruct the last slice:
```bash
nabu nabu.conf --slice last
```

### Reconstruct a range of slices

Reconstruct 300 slices (900, 901, ..., 1199 included):
```bash
nabu nabu.conf --slice 900-1199
```


### Reconstruct all the volume

If `start_z = 0` and `end_z = -1` in `nabu.conf`:
```bash
nabu nabu.conf
```
or equivalently:
```bash
nabu nabu.conf --slice all
```

### Tune the computing resources

If other processes are using memory on the local machine, then it is wise to restrict Nabu resources consumption. For this you can use `--cpu_mem_fraction` and `--gpu_mem_fraction`. By default these values are 0.9 (i.e 90% of said resources are used by nabu).

```bash
nabu nabu.conf --cpu_mem_fraction 0.5 --gpu_mem_fraction 0.6
```

### Define a log file

You can choose in which file the reconstruction messages (logs) are written. By default it is `nabu.log`.

```bash
nabu nabu.conf --logfile my_log_file.log
```


The verbosity can be tuned in the configuration file in section `[about]`.




