nabu.estimation package
=======================

Submodules
----------

.. toctree::
   :maxdepth: 4

   nabu.estimation.alignment
   nabu.estimation.cor
   nabu.estimation.cor_sino
   nabu.estimation.distortion
   nabu.estimation.focus
   nabu.estimation.tilt
   nabu.estimation.translation
   nabu.estimation.utils

Module contents
---------------

.. automodule:: nabu.estimation
   :members:
   :undoc-members:
   :show-inheritance:
