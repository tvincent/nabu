nabu.resources.cli.rotate module
================================

.. automodule:: nabu.resources.cli.rotate
   :members:
   :undoc-members:
   :show-inheritance:
