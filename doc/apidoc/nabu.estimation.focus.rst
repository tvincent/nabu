nabu.estimation.focus module
============================

.. automodule:: nabu.estimation.focus
   :members:
   :undoc-members:
   :show-inheritance:
