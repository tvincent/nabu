nabu.estimation.translation module
==================================

.. automodule:: nabu.estimation.translation
   :members:
   :undoc-members:
   :show-inheritance:
