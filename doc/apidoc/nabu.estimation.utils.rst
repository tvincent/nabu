nabu.estimation.utils module
============================

.. automodule:: nabu.estimation.utils
   :members:
   :undoc-members:
   :show-inheritance:
