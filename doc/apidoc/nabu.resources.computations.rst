nabu.resources.computations module
==================================

.. automodule:: nabu.resources.computations
   :members:
   :undoc-members:
   :show-inheritance:
