nabu.resources.cli.histogram module
===================================

.. automodule:: nabu.resources.cli.histogram
   :members:
   :undoc-members:
   :show-inheritance:
