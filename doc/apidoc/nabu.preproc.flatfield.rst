nabu.preproc.flatfield module
=============================

.. automodule:: nabu.preproc.flatfield
   :members:
   :undoc-members:
   :show-inheritance:
