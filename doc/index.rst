.. Nabu documentation master file, created by
   sphinx-quickstart on Thu Oct  3 19:50:27 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Nabu |version|
==============

Nabu is a tomography software used at the `European Synchrotron <https://www.esrf.eu>`_.


Quick links:

   * `Nabu development place <https://gitlab.esrf.fr/tomotools/nabu/>`_
   * Project maintainer: `Pierre Paleo <mailto:pierre.paleo@esrf.fr>`_

Latest news:

  * May 2021: Version 2021.1.0 released


Getting started
---------------

.. toctree::
   :maxdepth: 1
 
   install.md
   quickstart.md
   nabu_cli.md
   nabu_config_file.md
   nabu_config_items.md
   definitions.md
   nabu_validator_cli.md
   about.md


Features
--------
.. toctree::
   :maxdepth: 1

   features.md
   phase.md   
   estimation.md



Tutorials
---------

.. toctree::
   :maxdepth: 2

   tutorials.rst


Advanced documentation
-----------------------

.. toctree::
   :maxdepth: 1

   tests.md
   validators.md
   architecture1.md
   configparsing.md


API reference
--------------

.. toctree::
   :maxdepth: 3

   apidoc/nabu.rst


Release history
----------------

.. toctree::
   :maxdepth: 1


   v2021_1_0.md
   v2020_5_0.md
   v2020_4_0.md
   v2020_3_0.md
   v2020_2_0.md



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
