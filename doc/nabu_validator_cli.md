# Validator tool

## Goal

This application checks a dataset to ensure it can be reconstructed.

By default it will check that phase retrieval and reconstruction can be done.
This entails checking for default values and valid links (in the case of virtual HDF5 dataset).

## Usage

``` bash
$: nabu-validator [-h] [--ignore-dark] [--ignore-flat] [--no-phase-retrieval] [--check-nan] [--skip-links-check] [--all-entries] [--extend] path [entries [entries ...]]

Check if provided scan(s) seems valid to be reconstructed.

positional arguments:
  path                  Data to validate (h5 file, edf folder)
  entries               Entries to be validated (in the case of a h5 file)

optional arguments:
  -h, --help            show this help message and exit
  --ignore-dark         Do not check for dark
  --ignore-flat         Do not check for flat
  --no-phase-retrieval  Check scan energy, distance and pixel size
  --check-nan           Check frames if contains any nan.
  --skip-links-check, --no-link-check
                        Check frames dataset if have some broken links.
  --all-entries         Check all entries of the files (for HDF5 only for now)
  --extend              By default it only display items with issues. Extend will display them all
```

## Example

On the `bamboo_hercules` dataset some data has not been copied. This ends up with the following output:

``` bash
$: nabu-validator bambou_hercules_0001_1_1.nx --extend
```

``` bash
💥💣💥
 3 issues found from hdf5 scan(master_file: bamboo_hercules/bambou_hercules_0001/bambou_hercules_0001_1_1.nx, entry: entry0000)
   - projection(s)  : INVALID - At least one dataset seems to have broken link
   - dark(s)        : INVALID - At least one dataset seems to have broken link
   - flat(s)        : INVALID - At least one dataset seems to have broken link
   + distance       :  VALID
   + energy         :  VALID
   + pixel size     :  VALID
```
Same example but with all related data copied (link):

``` bash
👌👍👌
 No issue found from hdf5 scan(master_file: bamboo_hercules/bambou_hercules_0001/bambou_hercules_0001_1_1.nx, entry: entry0000).
   + projection(s)  :  VALID
   + dark(s)        :  VALID
   + flat(s)        :  VALID
   + distance       :  VALID
   + energy         :  VALID
   + pixel size     :  VALID
```
