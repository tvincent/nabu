# Installation

To install the current release:
```
pip install nabu
```

To install the current development version
```bash
pip install git+https://gitlab.esrf.fr/tomotools/nabu.git
```

## Dependencies

The above `pip install`  command should automatically install the nabu dependencies.

There are optional dependencides enabling various features:
  - Computations acceleration (GPU/manycore CPU): `pycuda`, `scikit-cuda`
  - Image processing and fitting utils: `scipy`, `skimage`

Please note that Nabu supports Python >= 3.5.
