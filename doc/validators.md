# Configuration validator

```{note}
This page is intended for Nabu developers.
```

## Principle

Nabu is usually used as a program through the command-line interface. 
The entry point for the user is the [Nabu configuration file](nabu_config_file.md). 
From the Nabu configuration file and the input dataset, Nabu extracts all the information necessary for the processing.

The following diagram explains how the internal Nabu configuration is built from these informations.


![validators](images/validators.png)

The "Converter" component exists solely to maintain the compatibility with PyHST2 `.par` files. 

## Configuration file validator

The configuration file is parsed and each value undergoes a check (named validator). For example, some values can be allowed to be negative, while some others values must be part of a range of acceptable values, etc. Therefore, each key has its validator. 

These validators are implemented in the module `nabu.resources.validators` and are decorated with `@validator`.

For example, a validator for non-negative integers:

```python
@validator
def nonnegative_integer_validator(val):
    val_int, error = convert_to_int(val)
    assert error is None and val_int >= 0, "number must be a non-negative integer"
    return val_int
```

The `@validator` decorator is used to indicate the name of the section and key name if an assertion fails (i.e if a value is invalid).

The configuration file validators is a first check. At this point no information on the dataset (like number of rows/columns in the files, pixel size, energy, etc) is available. 

## Dataset analyzer

The user-provided dataset is analyzed by the "Dataset analyzer" component. 
A dataset is either
 - A HDF5 master file following the [Nexus/NXtomo](http://download.nexusformat.org/doc/html/classes/applications/NXtomo.html) convention (preferred format)
 - A path to EDF and `.info` files (legacy)

From the dataset, many parameters are extracted and put in a dictionary.

```{important}
All the parameters that can be retrieved from the dataset should be avoided in the configuration file. The purpose of the "Dataset Analyser" component is to avoid filling too much the configuration file. For example, the number of pixels in each dimension is not relevant in the configuration file.
```

## Validator, second stage

At this point, we have two dictionaries of parameters: one from the parsing/validation of the configuration file, and one from the dataset analyzer.
