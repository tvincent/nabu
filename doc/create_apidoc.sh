#!/bin/bash
rm -rf apidoc
sphinx-apidoc --separate --ext-autodoc -o apidoc .. ../*/tests/* ../*/distributed/*
rm -f apidoc/modules.rst
rm -f apidoc/setup.rst
sed -i '1s/.*/API reference/' apidoc/nabu.rst
sed -i '2s/.*/==============/' apidoc/nabu.rst
