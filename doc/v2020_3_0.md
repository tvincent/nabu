# Version 2020.3.0


Version 2020.3.0 is a major release for ESRF User Service Mode restart.

```{note}
The changelog is available at https://gitlab.esrf.fr/tomotools/nabu/-/blob/master/CHANGELOG.md
```

## Highlights

This section highlights some of the available [features](features.md).

### Full volume reconstruction

Nabu can now reconstruct a full volume from the command line. By default, `nabu nabu.conf` will reconstruct all the volume (except if `start_z` end/or `end_z` were modified in `nabu.conf`).  You can also do `nabu nabu.conf --slice all`. More information in the [nabu command line documentation](nabu_cli.md)

The volume is automatically divided in sub-volumes. Each sub-volume is reconstructed and saved in a HDF5 file. Then, all the sub-volumes are "stitched" together, and a HDF5 master file is created for the full volume.


### Alignment tools

A variety of alignment functions is now available in `nabu.preproc.alignment`.

### Tiff and JPEG2000 basic support

The default output file format is HDF5. However, the reconstructions can now be saved in tiff and jpeg2000. In this case, there will be one file per reconstructed slice.

Mind that the current version has the following limitations:
  - Tiff: only float32 data is saved (no normalization)
  - Jpeg2000: only lossless compression is supported
  - Jpeg2000: normalization to uint16 is done slice-wise, instead of volume-wise

These limitations should be lifted in a future release.
