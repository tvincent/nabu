__version__ = "2021.2.0-alpha3"
__nabu_modules__ = [
    "app",
    "cuda",
    "estimation",
    "io",
    "misc",
    "opencl",
    "pipeline",
    "preproc",
    "reconstruction",
    "resources",
    "thirdparty",
]
version = __version__
